# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
"""
SUS base guardian module

This defines the behavior for all suspensions.

"""
##################################################

from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
import sustools
import susconst
from burt import readBurt

import time

##################################################

nominal = 'ALIGNED'

ca_monitor = False
#ca_monitor_notify = False

##################################################

__, OPTIC = SYSTEM.split('_')

susobj = sustools.Sus(OPTIC)

##################################################

# Check for tripped WDs
def is_tripped():
    trippedwds = susobj.trippedWds()
    #trippedwds = susobj.trippedWds(susobj.levels()[0] + ['USER'])

    # if we're not tripped, return immediately
    if not trippedwds:
        return False

    # notify the user which WDs are tripped
    notify("WD tripped! operator reset required: %s" % (','.join(trippedwds)))

    return True

def test_offsets_engaged(trueorfalse):
    """Check if the test offsets we use for alignment are engaged or not.
    the argument allows us to use this for aligment and misalignment.
    If trueorfalse =
    True     Will return True if the banks are engaged
    False    Will return True if the banks are NOT engaged
    """
    switches = susobj.testOffsetSwitchRead(chans=susconst.alignment_dofs, levels=[susobj.levels()[0]])
    if len(set(switches)) > 1:
        return False
    elif trueorfalse == True:
        return all(switches)
    elif trueorfalse == False:
        return not all(switches)

def burt_restore_safe():
    # FIXME: this function is not ready yet for use yet.  The
    # ezca.burtwb() takes far too long to execute, and is not robust
    # (doesn't handle certain string values yet)
    return

    ifo = IFO.lower()
    optic = OPTIC.lower()

    # generate the path to the offset snapshot file
    sfile = '%ssus%s_safe.snap' % (ifo, optic)
    # The userapps_path() function would return the path relative to
    # the USERAPPS_PATH environment variable.  Guardian might be
    # pointing to its own checkout of USERAPPS.  However, we want the
    # offsets to be pulled from the most uptodate values, which are in
    # the main USERAPPS checkout in /opt/rtcds/userapps
    # FIXME: maybe change this to point to local checkout during science
    safepath = userapps_path('sus', ifo, 'burtfiles', sfile)
    log('BURT RESTORE SAFE: %s', safepath)
    ezca.burtwb(safepath)

# function to set the suspension back to it's "safe" configuration
def reset_safe():
    # FIXME: burt safe restore is not ready yet
    # burt_restore_safe()
    log('Turning off all LOCK outputs')
    susobj.lockOutputSwitchWrite('OFF')
    log('Turning off dither outputs')
    susobj.ditherOutputSwitchWrite('OFF')
    log('Turning off OPLEV damping outputs')
    susobj.olDampOutputSwitchWrite('OFF')
    log('Ramping down ALIGN offsets')
    engage_align_offsets('OFF')
    engage_misalign_offsets('OFF')
    log('Turning off ALIGN outputs')
    susobj.alignOutputSwitchWrite('OFF')
    susobj.testOutputSwitchWrite('OFF')
    log('Turning off DAMP outputs')
    susobj.dampOutputSwitchWrite('OFF')
    log('Turning off DARM_DAMP outputs')
    susobj.darmDampOutputSwitchWrite('OFF')
    log('Turning off DARM VIOLIN DAMP outputs')
    susobj.darmDampViolinOutputSwitchWrite('OFF')

def set_align_tramps(tramp):
    susobj.alignRampWrite(tramp)
    susobj.testRampWrite(tramp, chans=susconst.alignment_dofs, levels=[susobj.levels()[0]])
    # HACK: we need to give the front end time to process that the
    # TRAMP has changed, before setting the new values.  We found that
    # frequently the new offsets would be written and processed before
    # the TRAMP change had taken affect.
    time.sleep(0.2)

def engage_align_offsets(onoff):
    stages = [susobj.levels()[0]]
    # Do R0 at the same time for quads
    if 'R0' in susobj.levels():
        stages += ['R0']
    log('Ramping ALIGNMENT offsets %s' % (onoff))
    susobj.alignOffsetSwitchWrite(onoff, levels=stages)

def engage_misalign_offsets(onoff):
    log('Ramping MISALIGNMENT offsets %s' % (onoff))
    susobj.testOffsetSwitchWrite(onoff, chans=susconst.alignment_dofs, levels=[susobj.levels()[0]])

def set_misalign_offsets():
    """Set the value of the misalignment. This should be stored in the
    local susconst.py so we can make sure that we are setting to 
    consistent values. 
    This was added because we use the TEST bank for these misalign 
    values and for actual tests. If a test does not restore these vals
    properly, youre going to have a bad time.
    """
    log('Setting MISALIGNMENT offset values')
    for dof, val in susconst.misalign_values[OPTIC].items():
        susobj.testOffsetWrite(val, chans=[dof], levels=[susobj.levels()[0]])

def is_aligning():
    """Return True if either P or Y of OPTICALIGN or TEST OFFSET is ramping"""
    chans = susobj.alignPvs() + susobj.testPvs(chans=susconst.alignment_dofs)
    for chan in chans:
        if ezca.is_offset_ramping(chan):
            return True
    return False

def change_coildriver_state(level,state):
    """Set coil driver state"""
    chans = susobj.levelsensactbiopvs(levels=[level],withprefix='bare')
    for chan in chans:
        ezca.write(chan,state)

def set_ramping_matrix(matrix_setting_chans,matrix_chans,matching_states,level):
    #channel order
    # 1 1, 2 1, 3 1, 4 1, 
    # 1 2, 2 2, 3 2, 4 2
    # 1 3, 2 3, 3 3, 4 3
    l =susconst.matrix_values[OPTIC][level]['l']
    p =susconst.matrix_values[OPTIC][level]['p']
    y =susconst.matrix_values[OPTIC][level]['y']
    order_of_coils = ['UL','LL','UR','LR']
    if matching_states == 0: #UL
        values = [0,2*l,2*l,0,0,0,2*p,-2*p,0,-2*y,0,2*y]
    elif matching_states == 1: #LL
        values = [2*l,0,0,2*l,0,0,2*p,-2*p,-2*y,0,2*y,0]
    elif matching_states == 2: #UR
        values = [2*l,0,0,2*l,2*p,-2*p,0,0,0,-2*y,0,2*y]
    elif matching_states == 3: #LR
        values = [0,2*l,2*l,0,2*p,-2*p,0,0,-2*y,0,2*y,0]
    elif matching_states == 4: #Normal
        values = [l,l,l,l,p,-p,p,-p,-y,-y,y,y]
    #check if we are already there
    done = True
    for val,chan in zip(values,matrix_chans):
        log("Val = " + str(val) + " Chan = " + str(chan))
        if not (ezca.read(chan) == val):
            done = False
            break
    if done:
       log("We are in the final matrix state")
       #We can push the coil driver value, so return True     
       return True
    #Not done, so set the matrix values 
    for val,chan in zip(values,matrix_setting_chans):
        ezca.write(chan,val)
    #Make sure the ramp time is good, then hit the load button
    matrix_tramp_chan = matrix_setting_chans[0][:-11]+'TRAMP'
    matrix_load_chan = matrix_setting_chans[0][:-11]+'LOAD_MATRIX'
    ezca.write(matrix_tramp_chan,susconst.coildriver_ramp_time)
    time.sleep(0.2)
    ezca.write(matrix_load_chan,1)
    time.sleep(0.2)
    return False



def check_and_change_coildriver_state(level,state):
    """Check and return current coil driver and ESD states"""
    #Are we a ramping matrix? Do we have a defined matrix values? If so, wait to finish (return False)
    #Otherwise, just slam it in since we can't do it smoothly
    if (not (susobj.levelbiocontroltype(levels=[level]) == ['split'])) or (not (OPTIC in susconst.matrix_values)):
        #Not split actuator, so just set 
        log('Not a ramping matrix or else susconst.matrix_value not defined, pushing setting immediately')
        chans = susobj.levelsensactbiopvs(levels=[level])
        for chan in chans:
            ezca.write(chan,state)
        time.sleep(0.2)
        return True

    #Are we ramping?
    matrix_ramping_chans = susobj.levelsensactmatrixblockpvs('osemConfig','eul2',levels=[level],suffix='_RAMPING',withprefix='bare')
    for chan in matrix_ramping_chans:
        if ezca.read(chan) == 1:
            #We are currently ramping, so don't touch anything.  We are not done, so return False
            return False
    #What configuration are we in?  If in final configuration, done (return True)
    chans = susobj.levelsensactbiopvs(levels=[level])
    matching_states = 0
    for chan in chans:
        log("Checking chan " + str(chan))
        if ezca.read(chan) == state:
            matching_states = matching_states + 1
    log("Coildriver states already changed = " + str(matching_states))
    #Based on how many are correct, determines how far along the process we are
    matrix_setting_chans = susobj.levelsensactmatrixblockpvs('osemConfig','eul2',levels=[level],suffix='_SETTING',withprefix='bare')
    matrix_chans = susobj.levelsensactmatrixblockpvs('osemConfig','eul2',levels=[level],suffix='',withprefix='bare')
    if set_ramping_matrix(matrix_setting_chans,matrix_chans,matching_states,level):
        if matching_states == 4:
            return True
        else:
            time.sleep(2.0)
            ezca.write(chans[matching_states],state)
            #Give electronics some time to settle
            time.sleep(2.2)
    return False


##################################################
# STATE DECORATORS
##################################################

class watchdog_check(GuardStateDecorator):
    def pre_exec(self):
        if is_tripped():
            return 'TRIPPED'

class test_offset_off(GuardStateDecorator):
    def pre_exec(self):
        if not test_offsets_engaged(False):
            notify('Test offset(s) are engaged')

class test_offset_on(GuardStateDecorator):
    def pre_exec(self):
        if not test_offsets_engaged(True):
            notify('Test offset(s) are NOT engaged')

# FIXME: add more decorator checks:
# masterswitch_check
# damping_loops_check

##################################################
# STATE GENERATORS
##################################################

def get_aligning_state(align, misalign):
    class ALIGNING_STATE(GuardState):
        request = False

        @watchdog_check
        def main(self):
            # FIXME: we want to shut on/off the output of the LOCK filter banks when we're not aligned, currently we assume that the ISC guardians are doing the right thing and not sent control signals when we're misaligned.
            #if misalign == 'ON':
            #    susobj.lockOutputSwitchWrite('OFF')
            #else:
            #    susobj.lockOutputSwitchWrite('ON')
            set_align_tramps(susconst.align_ramp_time)
            engage_align_offsets(align)
            if misalign == 'ON':
                set_misalign_offsets()
            engage_misalign_offsets(misalign)

        @watchdog_check
        def run(self):
            if is_aligning():
                return
            set_align_tramps(susconst.default_ramp_time)
            return True

    return ALIGNING_STATE


def get_idle_state():
    class IDLE_STATE(GuardState):
        request = True

        @watchdog_check
        def run(self):
            return True

    return IDLE_STATE


def get_alignment_idle_state(test_engaged):
    """Idle state for MISALIGNED and ALIGNED states.
    test_engaged should be True or False depending on 
    what the Test offsets should be.
    """
    if test_engaged:
        class TEST_ENG_IDLE_STATE(GuardState):
            request = True

            @watchdog_check
            @test_offset_on
            def run(self):
                return True

        return TEST_ENG_IDLE_STATE
    else:
        class TEST_NOT_ENG_IDLE_STATE(GuardState):
            request = True

            @watchdog_check
            @test_offset_off
            def run(self):
                return True

        return TEST_NOT_ENG_IDLE_STATE


##################################################
# STATES
##################################################

class INIT(GuardState):
    @watchdog_check
    def main(self):

        set_align_tramps(susconst.align_ramp_time)

        if not susobj.masterSwitchRead():
            log('Resetting SAFE...')
            reset_safe()
            self.return_state = 'SAFE'

        else:
            # FIXME: should do more checks that filter outputs are
            # engaged, etc.
            if all(susobj.dampOutputSwitchRead()):
                if all(susobj.alignOffsetSwitchRead()):
                    if any(susobj.testOffsetSwitchRead()):
                        self.return_state = 'MISALIGNED'
                    else:
                        self.return_state = 'ALIGNED'

                else:
                    engage_align_offsets('OFF')
                    engage_misalign_offsets('OFF')
                    self.return_state = 'DAMPED'

            else:
                engage_align_offsets('OFF')
                engage_misalign_offsets('OFF')
                self.return_state = 'DAMPED'

        log("next state: %s" % self.return_state)

    @watchdog_check
    def run(self):
        if is_aligning():
            return
        set_align_tramps(susconst.default_ramp_time)
        if self.return_state == 'SAFE':
            log('Turning off Master Switch')
            susobj.masterSwitchWrite('OFF')
        return self.return_state


class TRIPPED(GuardState):
    index = 1
    request = False
    redirect = False

    def main(self):
        # For reporting trip status
        is_tripped()

    def run(self):
        # For reporting trip status
        is_tripped()

        # Finish the state if we're no longer tripped
        if not is_tripped():
            return True


class RESET(GuardState):
    index = 9
    goto = True
    request = False

    @watchdog_check
    def main(self):
        reset_safe()
    @watchdog_check
    def run(self):
        if is_aligning():
            return
        susobj.alignRampWrite(susconst.default_ramp_time)
        log('Turning off Master Switch')
        susobj.masterSwitchWrite('OFF')
        return True


SAFE = get_idle_state()
SAFE.index = 10


class MASTERSWITCH_ON(GuardState):
    index = 20
    request = False

    @watchdog_check
    def main(self):
        log('Turning On Master Switch')
        susobj.masterSwitchWrite('ON')
        return True


class ENGAGE_DAMPING(GuardState):
    index = 45
    request = False

    @watchdog_check
    def main(self):
        log('Turning on damping outputs')
        susobj.dampOutputSwitchWrite('ON')
        susobj.darmDampOutputSwitchWrite('ON')
        susobj.darmDampViolinOutputSwitchWrite('ON')
        return True


DAMPED = get_idle_state()
DAMPED.index = 50


class ENABLE_ALL(GuardState):
    index = 55
    request = False

    @watchdog_check
    def main(self):
        log('Turning on ALL outputs')
        susobj.lockOutputSwitchWrite('ON')
        susobj.alignOutputSwitchWrite('ON')
        susobj.ditherOutputSwitchWrite('ON')
        susobj.testOutputSwitchWrite('ON')
        log('Turning on OL damping outputs')
        susobj.olDampOutputSwitchWrite('ON')
        log('Turning on DARM VIOLIN DAMP outputs')
        susobj.darmDampViolinOutputSwitchWrite('ON')
        return True


FULLY_ENABLED = get_idle_state()
FULLY_ENABLED.index = 60


class DISABLE(GuardState):
    index = 59
    request = False

    @watchdog_check
    def main(self):
        log('Turning off ALL outputs')
        susobj.lockOutputSwitchWrite('OFF')
        susobj.alignOutputSwitchWrite('OFF')
        susobj.testOutputSwitchWrite('OFF')
        return True

class RESET_COILDRIVERS(GuardState):
    index = 119
    request = False

    @watchdog_check
    def main(self):
        #We assume we are done, check all states, and if not, set done to false
        #If nothing defined for this optic, do nothing
        if not (OPTIC in susconst.coilstate_lock_acq.keys()):
            return True
        #Loop through all defined stages
        for level,state in susconst.coilstate_lock_acq[OPTIC].items():
            change_coildriver_state(level,susconst.coilstate_lock_acq[OPTIC][level])
        #Give some time for EPICS values to actually change before shifting to next state
        time.sleep(0.2)
        return True

    @watchdog_check
    def run(self):
        return True


class COILDRIVER_LOCK_ACQ(GuardState):
    index = 120
    request = True

    @watchdog_check
    def run(self):
        return True

class SET_COILDRIVER_LOWNOISE_1(GuardState):
    index = 129
    request = False

    @watchdog_check
    def run(self):
        if not (OPTIC in susconst.coilstate_first_low_noise.keys()):
            return True
        all_done = True
        for level,state in susconst.coilstate_first_low_noise[OPTIC].items():
            #Lazy evaluation, make sure function call is first
            all_done = check_and_change_coildriver_state(level,state) and all_done
        return all_done

class COILDRIVER_LOWNOISE_1(GuardState):
    index = 130
    request = True

    @watchdog_check
    def run(self):
        return True

class SET_COILDRIVER_LOWNOISE_2(GuardState):
    index = 139
    request = False

    @watchdog_check
    def run(self):
        if not (OPTIC in susconst.coilstate_second_low_noise.keys()):
            return True
        all_done = True
        for level,state in susconst.coilstate_second_low_noise[OPTIC].items():
            all_done = check_and_change_coildriver_state(level,state) and all_done
        return all_done


class COILDRIVER_LOWNOISE_2(GuardState):
    index = 140
    request = True

    @watchdog_check
    def run(self):
        return True


ALIGNING = get_aligning_state(align='ON', misalign='OFF')
ALIGNING.index = 90


ALIGNED = get_alignment_idle_state(False)
ALIGNED.index = 100


UNALIGNING = get_aligning_state(align='OFF', misalign='OFF')
UNALIGNING.index = 99


MISALIGNING = get_aligning_state(align='ON', misalign='ON')
MISALIGNING.index = 109


MISALIGNED = get_alignment_idle_state(True)
MISALIGNED.index = 110

class PREP_ROBUST_DAMPED(GuardState):
    """Used for time with excessive ground motion to protect 
    the suspensions.
    """
    index = 51
    request = False

    @watchdog_check
    def main(self):
        log('Switching top-driver bio state to 1.0')
        susobj.bioStateRequestWrite(1.0)
        log('Increasing watchdog threshold')
        susobj.wdTripThresholdWrite(susconst.wd_thresholds[OPTIC]['high'])
        return True


ROBUST_DAMPED = get_idle_state()
ROBUST_DAMPED.index = 52

class REVERT_ROBUST_DAMPED(GuardState):
    """Simply bring it back to how it was.

    """
    index = 53
    request = False

    def main(self):
        log('Switching top-driver bio state to 2.0')
        susobj.bioStateRequestWrite(2.0)
        log('Decreasing watdog threshold')
        susobj.wdTripThresholdWrite(susconst.wd_thresholds[OPTIC]['nominal'])
        return True

##################################################
# EDGES
# these are the directed edges that connect the states
# ('FROM', 'TO')

edges = [
    ('TRIPPED', 'RESET'),
    ('INIT', 'SAFE'),
    ('RESET', 'SAFE'),
    ('SAFE', 'MASTERSWITCH_ON'),
    ('MASTERSWITCH_ON', 'ENGAGE_DAMPING'),
    ('ENGAGE_DAMPING', 'DAMPED'),
    ('DAMPED', 'PREP_ROBUST_DAMPED'),
    ('PREP_ROBUST_DAMPED', 'ROBUST_DAMPED'),
    ('ROBUST_DAMPED', 'REVERT_ROBUST_DAMPED'),
    ('REVERT_ROBUST_DAMPED', 'DAMPED'),
    ('DAMPED', 'ENABLE_ALL'),
    ('ENABLE_ALL', 'FULLY_ENABLED'),
    ('FULLY_ENABLED', 'DISABLE'),
    ('DISABLE', 'DAMPED'),
    ('FULLY_ENABLED', 'ALIGNING'),
    ('ALIGNING', 'ALIGNED'),
    ('ALIGNED', 'UNALIGNING'),
    ('ALIGNED', 'MISALIGNING'),
    ('FULLY_ENABLED', 'MISALIGNING'),
    ('MISALIGNING', 'MISALIGNED'),
    ('MISALIGNED', 'ALIGNING'),
    ('MISALIGNED', 'UNALIGNING'),
    ('UNALIGNING', 'FULLY_ENABLED'),
    ('ALIGNED','RESET_COILDRIVERS'),
    ('RESET_COILDRIVERS','COILDRIVER_LOCK_ACQ'),
    ('COILDRIVER_LOCK_ACQ','ALIGNED'),
    ('ALIGNED','SET_COILDRIVER_LOWNOISE_1'),
    ('SET_COILDRIVER_LOWNOISE_1','COILDRIVER_LOWNOISE_1'),
    ('COILDRIVER_LOWNOISE_1','ALIGNED'),
    ('ALIGNED','SET_COILDRIVER_LOWNOISE_2'),
    ('SET_COILDRIVER_LOWNOISE_2','COILDRIVER_LOWNOISE_2'),
    ('COILDRIVER_LOWNOISE_2','ALIGNED'),

]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
